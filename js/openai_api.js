/**
 * @file
 * Defines the behaviors needed for openai_api integration.
 */

(function ($, Drupal) {
  'use strict';

  // To understand behaviors, see https://drupal.org/node/756722#behaviors
  Drupal.behaviors.openai_api = {

  }

  $(document).ready(function ($) {

  }

}(jQuery, Drupal));
